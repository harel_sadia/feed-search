
/* global SearchUrlApp */

SearchUrlApp.controller('SearchController', function ($scope, $http, $location) {
  $scope.feedList = [];
  $scope.urlList = [];
  $scope.searchUrl = '';
  $scope.selectedUrl = $location.hash();
  console.log($scope.selectedUrl);

  if (localStorage.hasOwnProperty('feedList')
          && localStorage.hasOwnProperty('urlList')) {
    $scope.feedList = JSON.parse(localStorage.getItem('feedList'));
    $scope.urlList = JSON.parse(localStorage.getItem('urlList'));
  }

  $scope.chooseUrl = function (url) {
    $scope.selectedUrl = url;
    $location.hash(url);
    getCurrentFeed(url);
  };

  if ($scope.selectedUrl) {
    $scope.chooseUrl($scope.selectedUrl);
  }

  $scope.deleteFeed = function (i) {
    $scope.urlList.splice(i, 1);
    $scope.feedList.splice(i, 1);
  };

  $scope.search = function () {
    var searchUrl = $scope.searchUrl;
    $http({
      method: "GET",
      url: "https://api.rss2json.com/v1/api.json?rss_url=" + searchUrl
    }).then(function success(response) {
      var data = response.data;
      console.log(response);
      if (data.status === 'ok') {
        getSearchData(data);
      } else {
        $scope.error = data.message;
      }
    }, function error(response) {
      console.log(response);
    });
  };
  function getSearchData(data) {
    var url = data.feed.url;
    if ($scope.urlList.indexOf(url) === -1) {
      $scope.error = '';
      $scope.selectedUrl = url;
      $scope.currentFeed = data;
      $scope.feedList.unshift(data);
      $scope.urlList.unshift(url);
      $location.hash(url);
      saveFeedList();
    } else {
      $scope.error = 'Feed url already exist!';
    }
    console.log($scope.feedList);
  }
  function getCurrentFeed() {
    var currentFeed = {};
    var url = $scope.selectedUrl;
    angular.forEach($scope.feedList, function (feedData) {
      if (url === feedData.feed.url) {
        currentFeed = feedData;
        return true;
      }
    });
    $scope.currentFeed = currentFeed;
  }
  function saveFeedList() {
    localStorage.setItem('feedList', JSON.stringify($scope.feedList));
    localStorage.setItem('urlList', JSON.stringify($scope.urlList));
  }

});